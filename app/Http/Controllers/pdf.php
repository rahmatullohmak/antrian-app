<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Symfony\Contracts\Service\Attribute\Required;


class pdf extends Controller
{
    //
    public function index()
    {

        $rowCount = DB::table('data_pengunjung')->count();

        $rowCount += 1;
        return view('depan', ['nomer' => $rowCount]);

    }
    public function ct()
    {
        DB::table('data_pengunjung')->insert([
            'pengunjung' => '1'
        ]);

        $rowCount = DB::table('data_pengunjung')->count();

        return view('struk', ['nomer' => $rowCount]);

    }
    public function pdf()
    {
        $rowCount = DB::table('data_pengunjung')->count();

        return view('admin', ['nomer' => $rowCount]);

    }
    public function hapus()
    {
        $deleted = DB::table('data_pengunjung')->delete();
        return redirect('/admin');
    }
}
