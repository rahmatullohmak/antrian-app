<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulir dengan Tombol Submit</title>

    <!-- Styling untuk contoh ini (opsional) -->
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 20px;
            text-align: center;
        }

        form {
            display: inline-block;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        button {
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            font-size: 16px;
        }
    </style>
</head>

<body>
    <h1>Jumlah Pengunjung Harian</h1>
    <h2>{{ $nomer }}</h2>

    <!-- Formulir dengan Tombol Submit -->
    <form action="/delete" method="post">
        @method('delete')
        @csrf


        <button type="submit">Reset</button>
    </form>


    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf

            <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>


        </form>
    </div>

</body>

</html>
