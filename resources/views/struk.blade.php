<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css" />
    <style>
        body {
            font-family: 'Arial', sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 600px;
            margin: 50px auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: center;
        }

        .queue-list h1 {
            font-size: 100px;
        }

        .queue-list {
            margin-top: 20px;
        }

        .queue-items {
            list-style-type: none;
            padding: 0;
        }

        .queue-item {
            background-color: #f9f9f9;
            margin: 5px;
            padding: 10px;
            border-radius: 5px;
        }
    </style>
    <title>Halaman Antrian Rumah Sakit</title>
    <script>
        window.onload = function() {
            window.print();

            setTimeout(function() {
                window.location.href = "/";
            }, 3000)
        };
    </script>
</head>

<body>
    <div class="container">
        <h1>Antrian Rumah Sakit</h1>
        <div class="queue-list">
            <h2>Daftar Nomor Antrian Anda</h2>
            <h1>{{ $nomer }}</h1>
            <ul class="queue-items" id="queueItems">
                <!-- Daftar antrian akan ditampilkan di sini -->
            </ul>
        </div>
    </div>
</body>

</html>
