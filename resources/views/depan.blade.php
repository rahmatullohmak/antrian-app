<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Antrian Rumah Sakit</title>
    <!-- Font Awesome untuk ikon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
</head>

<style>
    body {
        font-family: 'Arial', sans-serif;
        background-color: #f4f4f4;
        margin: 0;
        padding: 0;
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    header {
        background-color: #049e1b;
        color: #fff;
        padding: 10px 0;
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .logo {
        display: flex;
        align-items: center;
    }

    .logo i {
        font-size: 24px;
        margin-right: 8px;
    }

    nav ul {
        list-style: none;
        display: flex;
    }

    nav ul li {
        margin-right: 20px;
    }

    nav a {
        text-decoration: none;
        color: #fff;
        font-weight: bold;
    }

    .container {
        text-align: center;
        background-color: #fff;
        padding: 20px;
        border-radius: 8px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        margin-top: 20px;
    }

    h1 {
        color: #333;
    }

    .queue-info {
        margin-top: 20px;
    }

    .queue-info p {
        margin-bottom: 10px;
    }

    .queue-number {
        font-size: 36px;
        font-weight: bold;
        color: #007bff;
    }

    .btn-next {
        background-color: #28a745;
        color: #fff;
        padding: 10px 20px;
        font-size: 16px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        margin-top: 20px;
        box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
        text-decoration: none;
    }

    .btn-next:hover {
        background-color: #218838;
    }
</style>

<body>


    @if (strpos(url()->current(), 'cetak') == false)
        <header>
            <div class="logo">
                <i class="fas fa-hospital"></i>
                <span>Rumah Sakit</span>
            </div>
            </div>
            <nav>
                <ul>
                    <li><a href="/login">Login</a></li>
                </ul>
            </nav>
        </header>
    @endif

    <!-- Konten utama -->

    {{-- <form action="/cetak" method="post">
        @csrf --}}
    <div class="container">
        <h1>Sistem Antrian Rumah Sakit</h1>
        <div class="queue-info">
            <p>Nomor Antrian:</p>
            <span class="queue-number">{{ $nomer }}</span>
        </div>


        <br>
        <a href="/cetak" class="btn-next"> cetak</a>
    </div>

    {{-- </form> --}}
</body>
<script></script>

</html>
