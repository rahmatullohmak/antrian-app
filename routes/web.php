<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\pdf;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('depan');
// });
// Route::get('/admin', function () {
//     return view('admin');
// });



Route::middleware(['guest'])->group(function () {
    // ...
    Route::get('/struk', function () {
        return view('struk');
    });
    Route::get('/login', function () {
        return view('loginn');
    });


    Route::get('/', [pdf::class, 'index']);

    Route::get('/cetak', [pdf::class, 'ct']);



});







Auth::routes();
// File: routes/web.php

// Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::middleware(['auth'])->group(function () {
    // ...



    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/admin', [App\Http\Controllers\pdf::class, 'pdf']);
    Route::delete('/delete', [App\Http\Controllers\pdf::class, 'hapus']);


});
